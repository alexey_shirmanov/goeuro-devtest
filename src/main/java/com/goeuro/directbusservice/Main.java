package com.goeuro.directbusservice;

import com.goeuro.directbusservice.dto.BusRouteResponse;
import com.goeuro.directbusservice.service.BusRouteService;
import com.goeuro.directbusservice.service.BusRouteServiceImpl;
import lombok.extern.log4j.Log4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import static java.lang.Integer.valueOf;
import static java.nio.file.Files.lines;
import static java.nio.file.Paths.get;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@SpringBootApplication
@RestController
@Log4j
public class Main {
    private static String path;

    public static void main(String... args) {
        if (args.length != 1) {
            log.error("can't find route file passed as argument" + Arrays.toString(args));
            throw new RuntimeException("failed to get route file name");
        }
        path = args[0];
        SpringApplication.run(Main.class, args);
    }

    @RequestMapping(path = "/api/direct", method = GET)
    public BusRouteResponse processRouteRequest(@RequestParam(required = true, name = "dep_sid") int dep_sid,
                                                @RequestParam(required = true, name = "arr_sid") int arr_sid) {
        return new BusRouteResponse(dep_sid, arr_sid, getBusRouteService().hasDirectRoute(dep_sid, arr_sid));
    }

    @Bean
    public BusRouteService getBusRouteService() {
        return new BusRouteServiceImpl(getRouteParser().getRoutesData());
    }

    @Bean
    protected RouteParser getRouteParser() {
        return new RouteParser(path);
    }

    private static final class RouteParser {
        public static final String SEPARATOR = " ";
        final Map<Integer, Set<Integer>> routes;

        public RouteParser(String path) {
            try {
                this.routes = lines(get(path)).
                        skip(1)//skip first line since it contains just number fo routes
                        .map(line -> line.split(SEPARATOR))
                        .collect(toMap(
                                line -> valueOf(line[0]),//routeId is used as key
                                line -> stream(line).
                                        skip(1).//skip routeId
                                        map(Integer::valueOf).collect(toSet())));
            } catch (IOException e) {
                log.error("failed to process routes file", e);
                throw new RuntimeException(e);
            }
        }

        public Map<Integer, Set<Integer>> getRoutesData() {
            return routes;
        }
    }
}
