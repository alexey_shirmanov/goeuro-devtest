package com.goeuro.directbusservice.service;

public interface BusRouteService {
    /**
     * @param departureId departure station identifier
     * @param arrivalId   arrival station identifier
     * @return true if stations with departureId and arrivalId have direct connection false otherwise
     */
    boolean hasDirectRoute(int departureId, int arrivalId);
}
