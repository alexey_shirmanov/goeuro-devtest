package com.goeuro.directbusservice.service;


import java.util.Map;
import java.util.Set;

/*
* implementation supposes that routes are bi-directional. if we a route 1 -> 2 -> 3 so it is the same as if 3 -> 2 -> 1
* so it is possible to go from 1 to 3 as well as from 3 to 1.
*
* It has time complexity O(n). Memory usage can be improved using object factory approach with caching
* eg each station id would be identically (==) the same Integer object and memory usage will linearly
* depends on number of different stations.
*
* */
public class BusRouteServiceImpl implements BusRouteService {
    private final Map<Integer, Set<Integer>> routes;

    /*
    * implementation requires a map where key is unique route identifier and value is set of stations in this route.
    * */
    public BusRouteServiceImpl(final Map<Integer, Set<Integer>> routes) {
        this.routes = routes;
    }

    /**
     * we don't need to handle the list of routes somehow we just check if at least one route is present.
     */
    @Override
    public boolean hasDirectRoute(final int departureId, final int arrivalId) {
        return routes.entrySet().stream().
                map(Map.Entry::getValue).
                filter(stations -> stations.contains(departureId) && stations.contains(arrivalId)).
                findFirst().
                isPresent();
    }
}
