package com.goeuro.directbusservice.service;

import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BusRouteServiceImplTest {
    @Test
    public void testHasDirectRoute() throws Exception {
        //TODO randomise input data.
        final HashMap<Integer, Set<Integer>> testData = new HashMap<Integer, Set<Integer>>() {{
            put(1, new HashSet<Integer>() {{
                add(1);
                add(2);
                add(3);
            }});
            put(2, new HashSet<Integer>() {{
                add(4);
                add(5);
                add(6);
            }});
        }};

        BusRouteService busRouteService = new BusRouteServiceImpl(testData);
        assertTrue(busRouteService.hasDirectRoute(1, 2));
        assertTrue(busRouteService.hasDirectRoute(4, 6));
        assertTrue(busRouteService.hasDirectRoute(3, 2));
        assertFalse(busRouteService.hasDirectRoute(3, 6));
    }

}